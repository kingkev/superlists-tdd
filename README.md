Test Driven Development With Python
====================================

Basic Testing Methodology
--------------------------

*   Write a Functional Test to describe the new functionality from the user’s point of view. 
*   Think about how to write code that can get the failing functional test to pass
*   Write the smallest amount of application code to get the unit tests to pass. 
*   Run Functional Tests to see if they pass

*Each line of production code we write should be tested by (at least) one of our unit tests.*

Functional vs Unit Tests
------------------------

*   Functional tests should help you build an application with the right functionality, and guarantee you never accidentally break it. 
*   Unit tests should help you to write code that’s clean and bug free.

Chapter 4
---------
Refactoring - improve the code without changing its functionality.

*   Don't test constants, e.g. Testing HTML as text instead of a template
*   When refactoring, work on either the code or the tests, but not both at once.

Chapter 5
---------

*   Red/Green/Refactor - Write a test and see it fail (Red), write some code to get it to pass (Green), then Refactor to 	 improve the implementation. 
*	Three strikes and refactor - A rule of thumb for when to remove duplication from code. When two pieces of code look 	very similar, it often pays to wait until you see a third use case, so that you’re more sure about what part of the 	code really is the common, re-usable part to refactor out. 
*	This is a marathon chapter - Maybe repeat to ensure the concepts stick